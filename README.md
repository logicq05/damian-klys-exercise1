Instrukcja uruchomienia:
Należy mieć zainstowaną Javę 8, włączyć aplikację np. w Intellij, dociągnąć zależności Maven'en, a następnie uruchomić aplikację poprzez metodę main.
Gdyby były problemy z używaniem Lomboka, możliwe, że trzeba włączyć annotation processing w ustawieniach IDE.
Aplikację można przetestować używając Swaggera pod adresem: http://localhost:8080/swagger-ui.html

Dodatkowe uwagi:

W aplikacji skupiłem się na dobrych message'ach podczas walidacji kosztem ilości requestów do bazy.
Można to też zrobić mniejszą ilościa requestów, lecz wtedy dokładność informacji przy obsłudze błędów byłaby gorsza.
Pozwoliłem sobie na dużo requestów, gdyż aplikacja nie ma wielu danych.

Testów do aplikacji nie pisałem. W "prawdziwej" aplikacji oczywiście byłyby dodane. Tak samo logowanie/rejestracja itp.

Ponieważ nie ma logowania to przy sprzedaży założyłem, że sprzedaż rejestrowana jest przez kogoś innego niż klienta sprzedającego, dlatego w requeście trzeba podać też clientId.
Gdyby client miał wysyłać requesta rejestrującego sprzedaż i byłaby autentykacja to podawanie clientId nie byłoby konieczne.


Opis zadania:

Zadanie 1
Opis

Jesteś zobowiązany dostarczyć oprogramowanie wspomagające pracę hurtowni farmaceutycznej.

Zadanie polega na stworzeniu aplikacji (wystarczy API REST + SwaggerUI), która udostępni następujące usługi:

Produkty

dodawanie

zwrócenie listy

Magazyn

dodanie produktu na stan

zwrócenie listy stanów

Klienci

dodawanie

zwrócenie listy

Sprzedaż

zarejestrowanie sprzedaży

wyświetlenie historii (dla klienta / dla produktu)

Uwagi
Aplikacja powinna:
posiadać podstawową walidacje i być dostosowana do obsługi błędów,
być możliwa do uruchomienia na komputerze z zainstalowaną javą (sugerujemy bazy in-memory).

Implementacja
Rozwiązanie powinno być zaimplementowane w języku Java (w wersji 8 lub wyżej).
Kod powinien przedstawiać dobre praktyki kodowania obiektowego.
Sugerujemy zastosowanie Java 8 (lub wyżej).
Dozwolone jest użycie bibliotek pomocniczych lub baz danych (relacyjne/nierelacyjne/baza w pamięci). W przypadku bazy danych należy zwrócić uwagę na łatwość uruchomienia projektu.  
Ocenie podlegać będzie jakość kodu oraz funkcjonalność i jakość samej aplikacji.
Możliwe wykorzystanie systemów ułatwiających budowania aplikacji (Maven lub Gradle) oraz wykorzystania SpringBoot.
Repozytorium powinno zawierać plik README.md który zawiera ogólny opis oraz sposób uruchomienia aplikacji.

Wysyłka rozwiązania
Preferowany sposób dostarczenia rozwiązania to link do repozytorium GIT. Może to być GitHub, GitLab lub hostowany git.
Najważniejsze, by można było zaciągnąć źródła wywołując polecenie git clone LINK.
Jeśli nie chcesz skorzystać z tej opcji, prosimy o link do pliku ZIP. Plik powinien być łatwy w identyfikacji i zawierać imię i nazwisko.
