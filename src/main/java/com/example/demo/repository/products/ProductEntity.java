package com.example.demo.repository.products;

import com.example.demo.business.objects.products.ProductDto;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
@NoArgsConstructor
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;


    public ProductEntity(@NotNull String name) {
        this.name = name;
    }

    public ProductDto toDto() {
        return new ProductDto(id, name);
    }
}