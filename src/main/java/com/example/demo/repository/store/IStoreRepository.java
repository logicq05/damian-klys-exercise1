package com.example.demo.repository.store;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IStoreRepository extends JpaRepository<StoreStateEntity, Long> {

    Optional<StoreStateEntity> findByProductId(Long productId);

    Optional<StoreStateEntity> getStoreStateByProductId(Long productId);

    void deleteByProductId(Long productId);
}
