package com.example.demo.repository.store;

import com.example.demo.business.objects.store.StoreStateDto;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@NoArgsConstructor
public class StoreStateEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Long productId;

    @Min(0)
    @NotNull
    private Integer count;

    @Version
    private long version;


    public StoreStateEntity(@NotNull Long productId, @NotNull Integer productsCountToAdd) {
        this.productId = productId;
        this.count = productsCountToAdd;
    }

    public StoreStateDto toDto() {
        return new StoreStateDto(id, productId, count);
    }

    public void increaseCount(@NotNull Integer productsCountToAdd) {
        count += productsCountToAdd;
    }

    public void decreaseCountBy(Integer productsCountToRemove) {
        count -= productsCountToRemove;
    }
}