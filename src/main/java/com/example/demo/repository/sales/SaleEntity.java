package com.example.demo.repository.sales;

import com.example.demo.business.objects.sales.SaleDto;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
public class SaleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Long productId;

    @NotNull
    private Long clientId;

    @Min(0)
    @NotNull
    private Integer boughtProductsCount;

    @DateTimeFormat
    private LocalDateTime creationDateTime;

    @Version
    private long version;


    public SaleEntity(@NotNull Long productId, @NotNull Long clientId, @Min(0) @NotNull Integer boughtProductsCount) {
        this.productId = productId;
        this.clientId = clientId;
        this.boughtProductsCount = boughtProductsCount;
        creationDateTime = LocalDateTime.now();
    }

    public SaleDto toDto() {
            return new SaleDto(id, productId, clientId, boughtProductsCount, creationDateTime);
        }
}
