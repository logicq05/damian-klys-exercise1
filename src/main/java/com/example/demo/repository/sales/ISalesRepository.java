package com.example.demo.repository.sales;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ISalesRepository extends JpaRepository<SaleEntity, Long> {

    List<SaleEntity> findAllByClientIdOrderByCreationDateTimeDesc(Long clientId);

    List<SaleEntity> findAllByProductIdOrderByCreationDateTimeDesc(Long productId);
}
