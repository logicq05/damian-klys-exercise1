package com.example.demo.repository.clients;

import com.example.demo.business.objects.clients.ClientDto;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
@NoArgsConstructor
public class ClientEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String lastName;

    @NotNull
    private String email;


    public ClientEntity(@NotNull String name, @NotNull String lastName, @NotNull String email) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
    }

    public ClientDto toDto() {
        return new ClientDto(id, name, lastName, email);
    }
}
