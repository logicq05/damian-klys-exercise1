package com.example.demo.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableWebMvc
@Configuration
@ConfigurationProperties
public class CorsConfig implements WebMvcConfigurer {

    private static final String REGEX_SEPARATOR = ",\\s*";

    @Value("${endpoints.cors.allowed-origins}")
    private String allowedOrigins;

    @Value("${endpoints.cors.allowed-headers}")
    private String allowedHeaders;

    @Value("${endpoints.cors.allowed-methods}")
    private String allowedMethods;


    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins(allowedOrigins.split(REGEX_SEPARATOR))
                .allowedHeaders(allowedHeaders.split(REGEX_SEPARATOR))
                .allowedMethods(allowedMethods.split(REGEX_SEPARATOR));
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}