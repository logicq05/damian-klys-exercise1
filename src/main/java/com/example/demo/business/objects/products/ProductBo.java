package com.example.demo.business.objects.products;

import com.example.demo.repository.products.IProductRepository;
import com.example.demo.repository.products.ProductEntity;
import com.example.demo.restapi.products.request.RequestAddProduct;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

@Slf4j
@Component
@AllArgsConstructor
class ProductBo implements IProductBo {

    private final IProductRepository productRepository;

    @Override
    public ProductDto addProduct(RequestAddProduct requestAddProduct) {
        ProductEntity productEntity = new ProductEntity(requestAddProduct.getName());
        ProductEntity savedClientEntity = productRepository.save(productEntity);
        ProductDto productDto = savedClientEntity.toDto();
        log.trace("Product with id: {} has been successfully added to database.", productDto.getId());
        return productDto;
    }

    @Override
    public Optional<ProductDto> getProductByName(String name) {
        ProductEntity productEntity = productRepository.findOneByName(name);
        if(nonNull(productEntity)) {
            return Optional.ofNullable(productEntity.toDto());
        }
        return Optional.empty();
    }

    @Override
    public Optional<ProductDto> getProductById(Long id) {
        Optional<ProductEntity> productEntityOptional = productRepository.findById(id);
        return productEntityOptional.map(ProductEntity::toDto);
    }

    @Override
    public List<ProductDto> getAllProducts() {
        return productRepository.findAll()
                .stream()
                .map(ProductEntity::toDto)
                .collect(toList());
    }
}
