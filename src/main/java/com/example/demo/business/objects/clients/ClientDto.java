package com.example.demo.business.objects.clients;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ClientDto {

    private final Long id;
    private final String name;
    private final String lastName;
    private final String email;
}
