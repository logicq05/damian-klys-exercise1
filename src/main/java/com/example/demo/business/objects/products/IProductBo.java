package com.example.demo.business.objects.products;

import com.example.demo.restapi.products.request.RequestAddProduct;

import java.util.List;
import java.util.Optional;

public interface IProductBo {

    ProductDto addProduct(RequestAddProduct requestAddProduct);

    List<ProductDto> getAllProducts();

    Optional<ProductDto> getProductByName(String name);

    Optional<ProductDto> getProductById(Long productId);
}
