package com.example.demo.business.objects.store;

import com.example.demo.exceptions.NotEnoughProductsAvailableInStoreException;
import com.example.demo.exceptions.ProductMissingInStoreException;
import com.example.demo.repository.store.IStoreRepository;
import com.example.demo.repository.store.StoreStateEntity;
import com.example.demo.restapi.store.request.RequestAddProductToStore;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Slf4j
@Component
@AllArgsConstructor
class StoreBo implements IStoreBo {

    private final IStoreRepository storeRepository;

    @Override
    public StoreStateDto addProductOrIncreaseCount(RequestAddProductToStore requestAddProductToStore) {
        Optional<StoreStateEntity> storeStateEntityByProductId = storeRepository.findByProductId(requestAddProductToStore.getProductId());
        if(storeStateEntityByProductId.isPresent()) {
            return increaseCountOfProductsAndGetDto(requestAddProductToStore, storeStateEntityByProductId.get());
        } else {
            return addNewProductToStoreAndGetDto(requestAddProductToStore);
        }
    }

    private StoreStateDto increaseCountOfProductsAndGetDto(RequestAddProductToStore requestAddProductToStore, StoreStateEntity storeStateEntity) {
        storeStateEntity.increaseCount(requestAddProductToStore.getProductsCountToAdd());
        StoreStateEntity savedStoreStateEntity = storeRepository.save(storeStateEntity);
        StoreStateDto storeStateDto = savedStoreStateEntity.toDto();
        log.trace("Store state has been changed by increasing count of products with id: {}.", requestAddProductToStore.getProductId());
        return storeStateDto;
    }

    private StoreStateDto addNewProductToStoreAndGetDto(RequestAddProductToStore requestAddProductToStore) {
        StoreStateEntity storeStateEntity = new StoreStateEntity(requestAddProductToStore.getProductId(), requestAddProductToStore.getProductsCountToAdd());
        StoreStateEntity savedProductEntity = storeRepository.save(storeStateEntity);
        StoreStateDto productDto = savedProductEntity.toDto();
        log.trace("Store state has been changed by adding product with id: {}.", productDto.getId());
        return productDto;
    }

    @Override
    public List<StoreStateDto> getAllStoreStates() {
        return storeRepository.findAll()
                .stream()
                .map(StoreStateEntity::toDto)
                .collect(toList());
    }

    @Override
    public Optional<StoreStateDto> getStoreStateByProductId(Long productId) {
        Optional<StoreStateEntity> storeStateEntity = storeRepository.getStoreStateByProductId(productId);
        return storeStateEntity.map(StoreStateEntity::toDto);
    }

    @Override
    public void removeProductOrDecreaseCount(Long productId, Integer productsCountToRemove) {
        Optional<StoreStateEntity> storeStateEntityByProductId = storeRepository.findByProductId(productId);
        if(storeStateEntityByProductId.isPresent()) {
            int productsCountInStore = storeStateEntityByProductId.get().toDto().getCount();
            if(productsCountToRemove == productsCountInStore) {
                removeProductFromStore(productId);
            } else if (productsCountToRemove < productsCountInStore) {
                decreaseCountOfProducts(storeStateEntityByProductId.get(), productsCountInStore, productsCountToRemove, productId);
            } else {
                throw new NotEnoughProductsAvailableInStoreException("Cannot decrease products count from store!", productId, productsCountInStore, productsCountToRemove);
            }
        } else {
            throw new ProductMissingInStoreException("Cannot remove product from store!", productId);
        }
    }

    private void decreaseCountOfProducts(StoreStateEntity storeStateEntity, int productsCountInStore, Integer productsCountToRemove, Long productId) {
        if(productsCountInStore - productsCountToRemove < 0) {
            throw new IllegalStateException("Cannot decrease count by " + productsCountToRemove + ", because the final result is negative!");
        }
        storeStateEntity.decreaseCountBy(productsCountToRemove);
        log.trace("Store state has been changed by decreasing count of products with id: {}.", productId);
    }

    private void removeProductFromStore(Long productId) {
        storeRepository.deleteByProductId(productId);
        log.trace("Store state has been changed by deleting product with id: {}.", productId);
    }
}