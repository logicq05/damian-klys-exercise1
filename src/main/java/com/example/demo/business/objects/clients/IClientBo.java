package com.example.demo.business.objects.clients;

import com.example.demo.restapi.clients.request.RequestAddClient;

import java.util.List;
import java.util.Optional;

public interface IClientBo {

    ClientDto addClient(RequestAddClient requestClientAdd);

    List<ClientDto> getAllClients();

    Optional<ClientDto> getClientByEmail(String email);

    Optional<ClientDto> getClientById(Long clientId);
}
