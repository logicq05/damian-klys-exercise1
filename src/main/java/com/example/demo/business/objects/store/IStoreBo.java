package com.example.demo.business.objects.store;

import com.example.demo.restapi.store.request.RequestAddProductToStore;

import java.util.List;
import java.util.Optional;

public interface IStoreBo {

    StoreStateDto addProductOrIncreaseCount(RequestAddProductToStore requestAddProductToStore);

    List<StoreStateDto> getAllStoreStates();

    Optional<StoreStateDto> getStoreStateByProductId(Long productId);

    void removeProductOrDecreaseCount(Long productId, Integer productsCountToBuy);
}
