package com.example.demo.business.objects.sales;

import com.example.demo.repository.sales.ISalesRepository;
import com.example.demo.repository.sales.SaleEntity;
import com.example.demo.restapi.sales.request.RequestRegisterSale;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
@AllArgsConstructor
class SalesBo implements ISalesBo {

    private final ISalesRepository salesRepository;

    @Override
    public SaleDto registerSale(RequestRegisterSale requestRegisterSale) {
        SaleEntity saleEntity = new SaleEntity(requestRegisterSale.getProductId(), requestRegisterSale.getClientId(), requestRegisterSale.getProductsCountToBuy());
        SaleEntity savedSaleEntity = salesRepository.save(saleEntity);
        return savedSaleEntity.toDto();
    }

    @Override
    public List<SaleDto> getClientHistoryOfSales(Long clientId) {
        List<SaleEntity> clientHistoryOfSales = salesRepository.findAllByClientIdOrderByCreationDateTimeDesc(clientId);
        return mapSalesToDto(clientHistoryOfSales);
    }

    @Override
    public List<SaleDto> getProductHistoryOfSales(Long productId) {
        List<SaleEntity> productHistoryOfSales = salesRepository.findAllByProductIdOrderByCreationDateTimeDesc(productId);
        return mapSalesToDto(productHistoryOfSales);
    }

    private List<SaleDto> mapSalesToDto(List<SaleEntity> clientHistoryOfSales) {
        return clientHistoryOfSales.stream().map(SaleEntity::toDto).collect(toList());
    }
}
