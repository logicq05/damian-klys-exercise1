package com.example.demo.business.objects.products;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ProductDto {

    private final Long id;
    private final String name;
}
