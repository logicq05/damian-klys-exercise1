package com.example.demo.business.objects.sales;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
public class SaleDto {

    private final Long id;
    private final Long productId;
    private final Long clientId;
    private final Integer boughtProductsCount;
    private final LocalDateTime creationDateTime;
}
