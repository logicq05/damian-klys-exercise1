package com.example.demo.business.objects.store;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class StoreStateDto {

    private final Long id;
    private final Long productId;
    private final Integer count;
}
