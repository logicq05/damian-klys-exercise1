package com.example.demo.business.objects.sales;

import com.example.demo.restapi.sales.request.RequestRegisterSale;

import java.util.List;

public interface ISalesBo {

    SaleDto registerSale(RequestRegisterSale requestRegisterSale);

    List<SaleDto> getClientHistoryOfSales(Long clientId);

    List<SaleDto> getProductHistoryOfSales(Long productId);
}
