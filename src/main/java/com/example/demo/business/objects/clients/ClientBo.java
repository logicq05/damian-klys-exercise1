package com.example.demo.business.objects.clients;

import com.example.demo.repository.clients.ClientEntity;
import com.example.demo.repository.clients.IClientRepository;
import com.example.demo.restapi.clients.request.RequestAddClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

@Slf4j
@Component
@AllArgsConstructor
class ClientBo implements IClientBo {

    private final IClientRepository clientRepository;

    @Override
    public ClientDto addClient(RequestAddClient requestClientAdd) {
        ClientEntity clientEntity = new ClientEntity(requestClientAdd.getName(), requestClientAdd.getLastName(), requestClientAdd.getEmail());
        ClientEntity savedClientEntity = clientRepository.save(clientEntity);
        ClientDto clientDto = savedClientEntity.toDto();
        log.trace("Client with id: {} has been successfully added to database.", clientDto.getId());
        return clientDto;
    }

    @Override
    public List<ClientDto> getAllClients() {
        return clientRepository.findAll()
                .stream()
                .map(ClientEntity::toDto)
                .collect(toList());
    }

    @Override
    public Optional<ClientDto> getClientByEmail(String email) {
        ClientEntity clientEntity = clientRepository.findOneByEmail(email);
        if(nonNull(clientEntity)) {
            return Optional.ofNullable(clientEntity.toDto());
        }
        return Optional.empty();
    }

    @Override
    public Optional<ClientDto> getClientById(Long id) {
        return clientRepository.findById(id)
                .map(ClientEntity::toDto);
    }
}
