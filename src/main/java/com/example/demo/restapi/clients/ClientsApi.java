package com.example.demo.restapi.clients;

import com.example.demo.services.clients.IClientService;
import com.example.demo.restapi.clients.request.RequestAddClient;
import com.example.demo.restapi.clients.response.ResponseAddClient;
import com.example.demo.restapi.clients.response.ResponseGetAllClients;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Api(value = "Clients API")
@RestController
@AllArgsConstructor
class ClientsApi {

    private final IClientService clientService;


    @PostMapping(value = "/clients/add")
    @ApiOperation(value = "Adds a new client")
    ResponseEntity<ResponseAddClient> addClient(@Valid @NotNull @RequestBody RequestAddClient requestAddClient) {
        ResponseAddClient responseAddClient = clientService.addClient(requestAddClient);
        return ResponseEntity.status(HttpStatus.CREATED).body(responseAddClient);
    }

    @GetMapping(value = "/clients/list")
    @ApiOperation(value = "Get all clients")
    ResponseEntity<ResponseGetAllClients> getAllClients() {
        return ResponseEntity.ok(clientService.getAllClients());
    }
}