package com.example.demo.restapi.clients.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class ResponseGetAllClients {

    private final List<ResponseClient> clients;
}
