package com.example.demo.restapi.clients.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ResponseAddClient {

    private final ResponseClient client;
}