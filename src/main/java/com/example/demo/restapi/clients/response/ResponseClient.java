package com.example.demo.restapi.clients.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ResponseClient {

    private final Long id;
    private final String name;
    private final String lastName;
    private final String email;
}
