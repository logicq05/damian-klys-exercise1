package com.example.demo.restapi.sales.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class ResponseClientHistoryOfSales {

    private final List<ResponseRegisteredSale> registeredSales;
}
