package com.example.demo.restapi.sales.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
public class ResponseRegisteredSale {

    private final Long id;
    private final Long productId;
    private final Long clientId;
    private final Integer boughtProductsCount;

    @JsonFormat(pattern="yyyy-MM-dd:HH-mm-ss")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private final LocalDateTime creationDateTime;
}
