package com.example.demo.restapi.sales.request;

import lombok.Getter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
public class RequestRegisterSale {

    @NotNull
    private Long productId;
    @NotNull
    private Long clientId;
    @NotNull
    @Min(1)
    private Integer productsCountToBuy;
}