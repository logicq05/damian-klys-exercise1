package com.example.demo.restapi.sales;

import com.example.demo.restapi.sales.request.RequestRegisterSale;
import com.example.demo.restapi.sales.response.ResponseClientHistoryOfSales;
import com.example.demo.restapi.sales.response.ResponseProductHistoryOfSales;
import com.example.demo.restapi.sales.response.ResponseRegisteredSale;
import com.example.demo.services.sales.ISalesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Api(value = "Sales API")
@RestController
@AllArgsConstructor
class SalesApi {

    private final ISalesService salesService;

    @PostMapping(value = "/sales/add")
    @ApiOperation(value = "Register a new sale of product by client")
    ResponseEntity<ResponseRegisteredSale> registerSale(@Valid @NotNull @RequestBody RequestRegisterSale requestRegisterSale) {
        ResponseRegisteredSale responseRegisteredSale = salesService.registerSale(requestRegisterSale);
        return ResponseEntity.status(HttpStatus.CREATED).body(responseRegisteredSale);
    }

    @GetMapping(value = "/sales/list/client/{clientId}")
    @ApiOperation(value = "Gets history of sales for given client")
    ResponseEntity<ResponseClientHistoryOfSales> getClientHistoryOfSales(@Valid @NotNull @PathVariable Long clientId) {
        return ResponseEntity.ok(salesService.getClientHistoryOfSales(clientId));
    }

    @GetMapping(value = "/sales/list/product/{productId}")
    @ApiOperation(value = "Gets history of sales for given product")
    ResponseEntity<ResponseProductHistoryOfSales> getProductHistoryOfSales(@Valid @NotNull @PathVariable Long productId) {
        return ResponseEntity.ok(salesService.getProductHistoryOfSales(productId));
    }
}