package com.example.demo.restapi.store.request;

import lombok.Getter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
public class RequestAddProductToStore {

    @NotNull
    private Long productId;
    @NotNull
    @Min(1)
    private Integer productsCountToAdd;
}