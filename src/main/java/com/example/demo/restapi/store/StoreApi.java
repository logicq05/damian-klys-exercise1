package com.example.demo.restapi.store;

import com.example.demo.restapi.store.request.RequestAddProductToStore;
import com.example.demo.restapi.store.response.ResponseAddProductToStore;
import com.example.demo.restapi.store.response.ResponseGetAllStoreStates;
import com.example.demo.services.store.IStoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Api(value = "Store API")
@RestController
@AllArgsConstructor
class StoreApi {

    private final IStoreService storeService;

    @PostMapping(value = "/store/add")
    @ApiOperation(value = "Adds an existing product to store")
    ResponseEntity<ResponseAddProductToStore> addExistingProductToStore(@Valid @NotNull @RequestBody RequestAddProductToStore requestAddProductToStore) {
        ResponseAddProductToStore responseAddProductToStore = storeService.addProductToStore(requestAddProductToStore);
        return ResponseEntity.status(HttpStatus.OK).body(responseAddProductToStore);
    }

    @GetMapping(value = "/store/list")
    @ApiOperation(value = "Gets all products available in store")
    ResponseEntity<ResponseGetAllStoreStates> getAllStoreStates() {
        return ResponseEntity.ok(storeService.getAllStoreStates());
    }
}