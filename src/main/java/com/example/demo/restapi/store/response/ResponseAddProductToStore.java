package com.example.demo.restapi.store.response;

import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public class ResponseAddProductToStore {

    private ResponseStoreState responseStoreState;
}
