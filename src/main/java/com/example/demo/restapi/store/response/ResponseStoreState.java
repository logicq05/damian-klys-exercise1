package com.example.demo.restapi.store.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;


@Getter
@AllArgsConstructor
public class ResponseStoreState {

    @NotNull
    private final Long id;
    @NotNull
    private final Long productId;
    @NotNull
    private final Integer count;
}
