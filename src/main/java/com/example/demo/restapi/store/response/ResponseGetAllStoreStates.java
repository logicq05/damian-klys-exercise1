package com.example.demo.restapi.store.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;


@Getter
@AllArgsConstructor
public class ResponseGetAllStoreStates {

    private final List<ResponseStoreState> storeStates;
}
