package com.example.demo.restapi.products.response;

import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public class ResponseAddProduct {

    private final ResponseProduct product;
}
