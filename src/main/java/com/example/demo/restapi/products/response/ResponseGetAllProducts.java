package com.example.demo.restapi.products.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;


@Getter
@AllArgsConstructor
public class ResponseGetAllProducts {

    private final List<ResponseProduct> products;
}
