package com.example.demo.restapi.products.request;

import lombok.Getter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
public class RequestAddProduct {

    @NotNull
    @NotEmpty
    private String name;
}