package com.example.demo.restapi.products.response;

import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public class ResponseProduct {

    private final Long id;
    private final String name;
}
