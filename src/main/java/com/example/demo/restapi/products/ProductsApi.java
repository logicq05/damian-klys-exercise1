package com.example.demo.restapi.products;

import com.example.demo.restapi.products.request.RequestAddProduct;
import com.example.demo.restapi.products.response.ResponseAddProduct;
import com.example.demo.restapi.products.response.ResponseGetAllProducts;
import com.example.demo.services.products.IProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Api(value = "Products API")
@RestController
@AllArgsConstructor
class ProductsApi {

    private final IProductService productService;

    @PostMapping(value = "/products/add")
    @ApiOperation(value = "Adds a new product")
    ResponseEntity<ResponseAddProduct> addProduct(@Valid @NotNull @RequestBody RequestAddProduct requestAddProduct) {
        ResponseAddProduct responseAddProduct = productService.addProduct(requestAddProduct);
        return ResponseEntity.status(HttpStatus.CREATED).body(responseAddProduct);
    }

    @GetMapping(value = "/products/list")
    @ApiOperation(value = "Gets all products")
    ResponseEntity<ResponseGetAllProducts> getAllProducts() {
        return ResponseEntity.ok(productService.getAllProducts());
    }
}