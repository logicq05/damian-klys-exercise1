package com.example.demo.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@ResponseStatus(NOT_FOUND)
public class NotExistingClientException extends RuntimeException {

    public NotExistingClientException(String basicMessage, Long clientId) {
        super(basicMessage + " Client with id: " + clientId + " does not exist!");
    }
}