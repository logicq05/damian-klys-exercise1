package com.example.demo.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@ResponseStatus(NOT_FOUND)
public class NotExistingProductException extends RuntimeException {

    public NotExistingProductException(String basicMessage, Long productId) {
        super(basicMessage + " Product with id: " + productId + " does not exist!");
    }
}
