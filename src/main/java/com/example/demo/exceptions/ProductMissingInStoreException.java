package com.example.demo.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@ResponseStatus(NOT_FOUND)
public class ProductMissingInStoreException extends RuntimeException {

    public ProductMissingInStoreException(String basicMessage, Long productId) {
        super(basicMessage + " Product with id: " + productId + " is missing in store!");
    }
}
