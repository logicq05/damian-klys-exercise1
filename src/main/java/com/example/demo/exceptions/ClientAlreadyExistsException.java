package com.example.demo.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.CONFLICT;

@ResponseStatus(CONFLICT)
public class ClientAlreadyExistsException extends RuntimeException {

    public ClientAlreadyExistsException(String basicMessage, String clientsEmail) {
        super(basicMessage + " User with email: " + clientsEmail + " already exists!");
    }
}
