package com.example.demo.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ResponseStatus(BAD_REQUEST)
public class NotEnoughProductsAvailableInStoreException extends RuntimeException {

    public NotEnoughProductsAvailableInStoreException(String basicMessage, Long productId, int availableProductsCount, int requiredProductsCount) {
        super(basicMessage + " Not enough count of products with id: " + productId + " available in store! Available: " + availableProductsCount + ". Required: " + requiredProductsCount + ".");
    }
}
