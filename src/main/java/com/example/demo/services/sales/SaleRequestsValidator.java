package com.example.demo.services.sales;

import com.example.demo.business.objects.clients.ClientDto;
import com.example.demo.business.objects.clients.IClientBo;
import com.example.demo.business.objects.products.IProductBo;
import com.example.demo.business.objects.products.ProductDto;
import com.example.demo.business.objects.store.IStoreBo;
import com.example.demo.business.objects.store.StoreStateDto;
import com.example.demo.exceptions.NotEnoughProductsAvailableInStoreException;
import com.example.demo.exceptions.NotExistingClientException;
import com.example.demo.exceptions.NotExistingProductException;
import com.example.demo.exceptions.ProductMissingInStoreException;
import com.example.demo.restapi.sales.request.RequestRegisterSale;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@AllArgsConstructor
class SaleRequestsValidator {

    private static final String SALE_REGISTRATION_ERROR_MESSAGE = "Cannot register a new sale!";

    private final IClientBo clientBo;
    private final IProductBo productBo;
    private final IStoreBo storeBo;


    public void validate(RequestRegisterSale requestRegisterSale) {
        throwExceptionIfClientDoesNotExist(SALE_REGISTRATION_ERROR_MESSAGE, requestRegisterSale.getClientId());
        throwExceptionIfProductDoesNotExist(SALE_REGISTRATION_ERROR_MESSAGE, requestRegisterSale.getProductId());
        throwExceptionIfProductNotAvailableInStore(requestRegisterSale);
    }

    void throwExceptionIfClientDoesNotExist(String basicErrorMessage, Long clientId) {
        Optional<ClientDto> clientDtoById = clientBo.getClientById(clientId);
        if(!clientDtoById.isPresent()) {
            throw new NotExistingClientException(basicErrorMessage, clientId);
        }
    }

    void throwExceptionIfProductDoesNotExist(String basicErrorMessage, Long productId) {
        Optional<ProductDto> productDtoById = productBo.getProductById(productId);
        if(!productDtoById.isPresent()) {
            throw new NotExistingProductException(basicErrorMessage, productId);
        }
    }

    private void throwExceptionIfProductNotAvailableInStore(RequestRegisterSale requestRegisterSale) {
        Optional<StoreStateDto> storeStateDto = storeBo.getStoreStateByProductId(requestRegisterSale.getProductId());
        if(storeStateDto.isPresent()) {
            int productsCountInStore = storeStateDto.get().getCount();
            if(requestRegisterSale.getProductsCountToBuy() > productsCountInStore) {
                throw new NotEnoughProductsAvailableInStoreException(SALE_REGISTRATION_ERROR_MESSAGE,
                        requestRegisterSale.getProductId(),
                        productsCountInStore,
                        requestRegisterSale.getProductsCountToBuy());
            }
        } else {
            throw new ProductMissingInStoreException(SALE_REGISTRATION_ERROR_MESSAGE, requestRegisterSale.getProductId());
        }
    }
}