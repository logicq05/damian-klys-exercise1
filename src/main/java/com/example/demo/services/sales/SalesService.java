package com.example.demo.services.sales;

import com.example.demo.business.objects.sales.ISalesBo;
import com.example.demo.business.objects.sales.SaleDto;
import com.example.demo.business.objects.store.IStoreBo;
import com.example.demo.restapi.sales.request.RequestRegisterSale;
import com.example.demo.restapi.sales.response.ResponseClientHistoryOfSales;
import com.example.demo.restapi.sales.response.ResponseProductHistoryOfSales;
import com.example.demo.restapi.sales.response.ResponseRegisteredSale;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Slf4j
@Service
@Transactional
@AllArgsConstructor
class SalesService implements ISalesService {

    private final IStoreBo storeBo;
    private final ISalesBo salesBo;
    private final SaleRequestsValidator saleRequestsValidator;


    @Override
    public ResponseRegisteredSale registerSale(RequestRegisterSale requestRegisterSale) {
        saleRequestsValidator.validate(requestRegisterSale);
        storeBo.removeProductOrDecreaseCount(requestRegisterSale.getProductId(), requestRegisterSale.getProductsCountToBuy());
        SaleDto saleDto = salesBo.registerSale(requestRegisterSale);
        log.trace("Sale with id: {} has been successfully registered and added to database.", saleDto.getId());
        return new ResponseRegisteredSale(saleDto.getId(), saleDto.getProductId(), saleDto.getClientId(), saleDto.getBoughtProductsCount(), saleDto.getCreationDateTime());
    }

    @Override
    public ResponseClientHistoryOfSales getClientHistoryOfSales(Long clientId) {
        saleRequestsValidator.throwExceptionIfClientDoesNotExist("Cannot get history of sales for client!", clientId);
        List<SaleDto> salesDto = salesBo.getClientHistoryOfSales(clientId);
        return new ResponseClientHistoryOfSales(mapToResponseRegisteredSales(salesDto));
    }

    @Override
    public ResponseProductHistoryOfSales getProductHistoryOfSales(Long productId) {
        saleRequestsValidator.throwExceptionIfProductDoesNotExist("Cannot get history of sales for product!", productId);
        List<SaleDto> salesDto = salesBo.getProductHistoryOfSales(productId);
        return new ResponseProductHistoryOfSales(mapToResponseRegisteredSales(salesDto));
    }

    private List<ResponseRegisteredSale> mapToResponseRegisteredSales(List<SaleDto> salesDto) {
        return salesDto.stream()
                .map(saleDto -> new ResponseRegisteredSale(saleDto.getId(), saleDto.getProductId(), saleDto.getClientId(), saleDto.getBoughtProductsCount(), saleDto.getCreationDateTime()))
                .collect(toList());
    }
}
