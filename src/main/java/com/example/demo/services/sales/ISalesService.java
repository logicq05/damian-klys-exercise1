package com.example.demo.services.sales;

import com.example.demo.restapi.sales.request.RequestRegisterSale;
import com.example.demo.restapi.sales.response.ResponseClientHistoryOfSales;
import com.example.demo.restapi.sales.response.ResponseProductHistoryOfSales;
import com.example.demo.restapi.sales.response.ResponseRegisteredSale;

public interface ISalesService {

    ResponseRegisteredSale registerSale(RequestRegisterSale requestRegisterSale);

    ResponseClientHistoryOfSales getClientHistoryOfSales(Long clientId);

    ResponseProductHistoryOfSales getProductHistoryOfSales(Long productId);
}
