package com.example.demo.services.products;

import com.example.demo.business.objects.products.IProductBo;
import com.example.demo.business.objects.products.ProductDto;
import com.example.demo.restapi.products.request.RequestAddProduct;
import com.example.demo.restapi.products.response.ResponseAddProduct;
import com.example.demo.restapi.products.response.ResponseGetAllProducts;
import com.example.demo.restapi.products.response.ResponseProduct;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
@AllArgsConstructor
class ProductService implements IProductService {

    private final IProductBo productBo;
    private final ProductRequestsValidator productRequestsValidator;

    @Override
    public ResponseAddProduct addProduct(RequestAddProduct requestAddProduct) {
        productRequestsValidator.validate(requestAddProduct);
        ProductDto productDto = productBo.addProduct(requestAddProduct);
        return new ResponseAddProduct(mapToResponseProduct(productDto));
    }

    @Override
    public ResponseGetAllProducts getAllProducts() {
        List<ProductDto> productsDto = productBo.getAllProducts();
        List<ResponseProduct> responseProducts = productsDto.stream()
                .map(this::mapToResponseProduct)
                .collect(toList());
        return new ResponseGetAllProducts(responseProducts);
    }

    private ResponseProduct mapToResponseProduct(ProductDto productDto) {
        return new ResponseProduct(productDto.getId(), productDto.getName());
    }
}

