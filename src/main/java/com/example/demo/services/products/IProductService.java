package com.example.demo.services.products;

import com.example.demo.restapi.products.request.RequestAddProduct;
import com.example.demo.restapi.products.response.ResponseAddProduct;
import com.example.demo.restapi.products.response.ResponseGetAllProducts;

public interface IProductService {

    ResponseAddProduct addProduct(RequestAddProduct requestAddProduct);

    ResponseGetAllProducts getAllProducts();
}