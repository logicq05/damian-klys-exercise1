package com.example.demo.services.products;

import com.example.demo.business.objects.products.IProductBo;
import com.example.demo.business.objects.products.ProductDto;
import com.example.demo.exceptions.ProductAlreadyExistsException;
import com.example.demo.restapi.products.request.RequestAddProduct;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@AllArgsConstructor
class ProductRequestsValidator {

    private final IProductBo productBo;


    public void validate(RequestAddProduct requestAddProduct) {
        throwExceptionIfProductAlreadyExists(requestAddProduct);
    }

    private void throwExceptionIfProductAlreadyExists(RequestAddProduct requestAddProduct) {
        Optional<ProductDto> productDtoByName = productBo.getProductByName(requestAddProduct.getName());
        if(productDtoByName.isPresent()) {
            throw new ProductAlreadyExistsException("Cannot create a new product!", requestAddProduct.getName());
        }
    }
}