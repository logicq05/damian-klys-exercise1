package com.example.demo.services.clients;

import com.example.demo.business.objects.clients.ClientDto;
import com.example.demo.business.objects.clients.IClientBo;
import com.example.demo.restapi.clients.request.RequestAddClient;
import com.example.demo.restapi.clients.response.ResponseAddClient;
import com.example.demo.restapi.clients.response.ResponseClient;
import com.example.demo.restapi.clients.response.ResponseGetAllClients;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
@AllArgsConstructor
class ClientService implements IClientService {

    private final IClientBo clientBo;
    private final ClientRequestsValidator clientRequestsValidator;


    @Override
    public ResponseAddClient addClient(RequestAddClient requestAddClient) {
        clientRequestsValidator.validate(requestAddClient);
        ClientDto clientDto = clientBo.addClient(requestAddClient);
        return new ResponseAddClient(mapToResponseClient(clientDto));
    }

    @Override
    public ResponseGetAllClients getAllClients() {
        List<ClientDto> clientsDto = clientBo.getAllClients();
        List<ResponseClient> responseClients = clientsDto.stream()
                .map(this::mapToResponseClient)
                .collect(toList());
        return new ResponseGetAllClients(responseClients);
    }

    private ResponseClient mapToResponseClient(ClientDto clientDto) {
        return new ResponseClient(clientDto.getId(),
                clientDto.getName(),
                clientDto.getLastName(),
                clientDto.getEmail());
    }
}
