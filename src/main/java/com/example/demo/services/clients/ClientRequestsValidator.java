package com.example.demo.services.clients;

import com.example.demo.business.objects.clients.ClientDto;
import com.example.demo.business.objects.clients.IClientBo;
import com.example.demo.exceptions.ClientAlreadyExistsException;
import com.example.demo.restapi.clients.request.RequestAddClient;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@AllArgsConstructor
class ClientRequestsValidator {

    private final IClientBo clientBo;


    public void validate(RequestAddClient requestAddClient) {
        throwExceptionIfUserAlreadyExists(requestAddClient);
    }

    private void throwExceptionIfUserAlreadyExists(RequestAddClient requestAddClient) {
        Optional<ClientDto> clientByEmail = clientBo.getClientByEmail(requestAddClient.getEmail());
        if(clientByEmail.isPresent()) {
            throw new ClientAlreadyExistsException("Cannot create a new client.", requestAddClient.getEmail());
        }
    }
}