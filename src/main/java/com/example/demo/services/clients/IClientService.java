package com.example.demo.services.clients;

import com.example.demo.restapi.clients.request.RequestAddClient;
import com.example.demo.restapi.clients.response.ResponseAddClient;
import com.example.demo.restapi.clients.response.ResponseGetAllClients;

public interface IClientService {

    ResponseAddClient addClient(RequestAddClient requestAddClient);

    ResponseGetAllClients getAllClients();
}
