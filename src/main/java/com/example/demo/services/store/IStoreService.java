package com.example.demo.services.store;

import com.example.demo.restapi.store.request.RequestAddProductToStore;
import com.example.demo.restapi.store.response.ResponseAddProductToStore;
import com.example.demo.restapi.store.response.ResponseGetAllStoreStates;

public interface IStoreService {

    ResponseAddProductToStore addProductToStore(RequestAddProductToStore requestAddProductToStore);

    ResponseGetAllStoreStates getAllStoreStates();
}
