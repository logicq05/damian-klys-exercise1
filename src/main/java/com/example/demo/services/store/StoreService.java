package com.example.demo.services.store;

import com.example.demo.business.objects.products.IProductBo;
import com.example.demo.business.objects.products.ProductDto;
import com.example.demo.business.objects.store.IStoreBo;
import com.example.demo.business.objects.store.StoreStateDto;
import com.example.demo.exceptions.NotExistingProductException;
import com.example.demo.restapi.store.request.RequestAddProductToStore;
import com.example.demo.restapi.store.response.ResponseAddProductToStore;
import com.example.demo.restapi.store.response.ResponseGetAllStoreStates;
import com.example.demo.restapi.store.response.ResponseStoreState;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
@AllArgsConstructor
class StoreService implements IStoreService {

    private final IStoreBo storeBo;
    private final IProductBo productBo;


    @Override
    public ResponseAddProductToStore addProductToStore(RequestAddProductToStore requestAddProductToStore) {
        Optional<ProductDto> productDtoOptional = productBo.getProductById(requestAddProductToStore.getProductId());
        if(productDtoOptional.isPresent()) {
            StoreStateDto storeStateDto = storeBo.addProductOrIncreaseCount(requestAddProductToStore);
            return new ResponseAddProductToStore(mapToResponseProduct(storeStateDto));
        } else {
            throw new NotExistingProductException("Cannot add product to store!", requestAddProductToStore.getProductId());
        }
    }

    @Override
    public ResponseGetAllStoreStates getAllStoreStates() {
        List<StoreStateDto> storeStatesDto = storeBo.getAllStoreStates();
        List<ResponseStoreState> responseStoreStates = storeStatesDto.stream()
                .map(this::mapToResponseProduct)
                .collect(toList());
        return new ResponseGetAllStoreStates(responseStoreStates);
    }

    private ResponseStoreState mapToResponseProduct(StoreStateDto storeStateDto) {
        return new ResponseStoreState(storeStateDto.getId(), storeStateDto.getProductId(), storeStateDto.getCount());
    }
}

